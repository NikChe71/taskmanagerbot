import os
import re
import telegram
#from io import BytesIO
from telegram import KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext.callbackqueryhandler import CallbackQueryHandler
import logging
from bottle import route, view, run, request


class TaskManagerBot():

    def __init__(self):
        self.updater = Updater(token = self.__token())
        self.dispatcher = self.updater.dispatcher
        
        self.TASKS = {}

    def bot_run(self):
        logging.basicConfig(format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level = logging.INFO)

        self.emojis()
        self.counter = 1

        # Add created command to CommandHandler pool.
        start_handler = CommandHandler('start', self.start)
        stop_handler = CommandHandler('stop', self.stop)
        set_task_handler = CommandHandler('set_task', self.set_task)
        get_task_handler = CommandHandler('get_task', self.get_task)
        delete_task_handler = CommandHandler('delete_task', self.delete_task, pass_args = True)
        task_menu_handler = CommandHandler('task_menu', self.task_menu)
        callback_handler = CallbackQueryHandler(self.button)
        message_handler = MessageHandler(Filters.text, self.message_bot)

        # Get commands ready to execute.
        handlers = [start_handler,
                    stop_handler,
                    set_task_handler,
                    get_task_handler,
                    delete_task_handler,
                    callback_handler,
                    task_menu_handler,
                    message_handler]

        # Execute all commands.
        [self.dispatcher.add_handler(hndlr) for hndlr in handlers]

        # Launching this bot.
        self.updater.start_polling()

    def emojis(self):
        self.emoji_dictionary = {"thunderstorm" : u'\U0001F4A8',
                             "drizzle" : u'\U0001F4A7',
                            "rain" : u'\U00002614',
                            "snowflake" : u'\U00002744',
                            "snowman" : u'\U000026C4',
                            "atmosphere" : u'\U0001F301',
                            "clearSky" : u'\U00002600',
                            "fewClouds" : u'\U000026C5',
                            "clouds" : u'\U00002601',
                            "hot" : u'\U0001F525',
                            "defaultEmoji" : u'\U0001F300',
                            "smile" : u'\U0001F603',
                            "heart" : u'\U00002764',
                            "done" : u'\U00002705',
                            "ghost" : u'\U0001F47B',
                            "brain" : u'\U0001F47E',
                            "gamepad" : u'\U0001F3AE'}

    # Private method for storing token.
    def __token(self):
        token = '545279160:AAFzRLbPD8-FDe_1gA_EwtYUo3-u6oE_ZL4'
        return token

    # Method for checking is bot ON or OFF.
    def check_bot(self):
        bot = telegram.Bot(token = '545279160:AAFzRLbPD8-FDe_1gA_EwtYUo3-u6oE_ZL4')
        print(bot.get_me())

    # Method for printing message in chat while button or command /start is pushed,
    def start(self, bot, update):
        bot.send_message(chat_id = update.message.chat_id, text = "Привет! \nЯ бот - планировщик задач! \n\nИспользуй комманды: \n\n /set_task - чтобы создать новые задачи. \n Просто вводи задачи через знак \'+\', чтобы добавить их к уже существующим. \n\n /get_task - чтобы посмотреть активные задачи. \n\n /task_menu - для того чтобы удалить уже выполненные задачи. \n Нажми на одну из них, и она исчезнет из списка.")

    # Method for breaking connection with API.
    def stop(self, bot, update):
        self.updater.stop()

    def delete_task(self, bot, update, args):
        unique_id = update.message.chat_id
        task2del = ' '.join(map(str, args))
        for TSK in self.TASKS[unique_id]:
                    if len(re.findall(task2del, TSK)) == 1:
                        self.TASKS[unique_id].remove(TSK)
                        print(TSK)

    def button(self, bot, update):
        query = update.callback_query
        unique_id = query.message.chat_id
        data = self.TASKS[unique_id][int(query.data)]
        
        bot.editMessageText(text = "{}{}\nЗадача выполнена.".format(data, self.emoji_dictionary["done"]),
                            chat_id = query.message.chat_id,
                            message_id = query.message.message_id)
        self.TASKS[unique_id].pop(int(query.data))
        self.TASKS[unique_id] = list(filter(('').__ne__, self.TASKS[unique_id]))

        # Beta function for returning menu after pressing a button.
        #self.get_task(bot, update)
        # Beta function for returning menu after pressing a button.
        
    def task_menu(self, bot, update):
        unique_id = update.message.chat_id

        button_list_row = [
            [InlineKeyboardButton(str(task_memb), callback_data = '{}'.format(num)) for num, task_memb in enumerate(self.TASKS[unique_id])]
            ]
        button_list = [[element] for element in button_list_row[0]]
        markup = InlineKeyboardMarkup(button_list)

        if len(self.TASKS[unique_id]) == 0:
            update.message.reply_text('Ни одной задачи!'+self.emoji_dictionary["ghost"])
        elif len(self.TASKS[unique_id]) >= 1:
            update.message.reply_text("{0}Меню задач{0}".format(self.emoji_dictionary["brain"]), reply_markup = markup)

    # Method for saving tasks.
    def set_task(self, bot, update):
        unique_id = update.message.chat_id
        self.TASKS[unique_id] = []

        bot.send_message(chat_id = update.message.chat_id, text = "Напиши, какие задачи ты планируешь выполнить в ближайшее время. Разделяй их плюсами." + '\n+ ' + self.emoji_dictionary["brain"] + ' + ' + self.emoji_dictionary["gamepad"])
        command_pattern = '/start|/stop|/set_task|/get_task|/delete_task'
        comm2del = re.compile(command_pattern)
        mess_text = comm2del.sub(r'', update.message.text)
        tasks = str(mess_text).split('+')
        for tsk in tasks:
            self.TASKS[unique_id].append(tsk)

    # Return message.
    def set_task_upd(self, update):
        update.message.reply_text("Задачи записаны." + self.emoji_dictionary["atmosphere"])

    # Method for showing tasks.
    def get_task(self, bot, update):
        unique_id = update.message.chat_id
        # Create empty string, join each element of the list been converted to string
        if len(self.TASKS[unique_id]) >= 1:
            prinTasks = '{}'.format('\n'.join(map(str, self.TASKS[unique_id])))
            update.message.reply_text('Задачи '+self.emoji_dictionary["ghost"]+':\n'+prinTasks)
        else:
            update.message.reply_text('\n'+'Задачи отсутствуют '+self.emoji_dictionary["ghost"]+'\n')

    # Message handler.
    def message_bot(self, bot, update):
        unique_id = update.message.chat_id
        greet = 'Привет|привет|Салют|салют|Здорово|здорово'
        set_task = '\+'
        delete_task = '\-'
        secret_phrase = 'Лиза любит Никиту'
        answer_counter = 'counter'

        if len(re.findall(greet, update.message.text)) > 0:
            bot.send_message(chat_id = update.message.chat_id, text = "Привет!" + self.emoji_dictionary['smile'])
        elif update.message.text == secret_phrase:
            bot.send_message(chat_id = update.message.chat_id, text = self.emoji_dictionary['heart']*self.counter)
            self.counter += 1
        elif answer_counter == update.message.text:
            bot.send_message(chat_id = update.message.chat_id, text = "Counter: " + str(self.counter))
        elif len(re.findall(set_task, update.message.text)) > 0:
            tasks = str(update.message.text).split('+')
            for tsk in tasks:
                self.TASKS[unique_id].append(tsk)
        elif len(re.findall(delete_task, update.message.text)) > 0:
            del_tasks = str(update.message.text).split('-')
            for del_tsk in del_tasks:
                for TSK in self.TASKS[unique_id]:
                    if len(re.findall(del_tsk, TSK)) == 1:
                        self.TASKS[unique_id].remove(TSK)
    
    def ret_bot_update(self):
        return self.update


TOKEN = '545279160:AAFzRLbPD8-FDe_1gA_EwtYUo3-u6oE_ZL4'
APPNAME = 'taskmbot'
port = int(os.getenv("PORT"))

@route('/setWebhook')
def setWebhook():
    bot = telegram.Bot(TOKEN)
    botWebhookResult = bot.setWebhook(webhook_url='https://facemetricdatasaver.cfapps.eu10.hana.ondemand.com/botHook'.format(APPNAME))
    #botWebhookResult = bot.setWebhook(webhook_url='https://taskmanagerbot.cfapps.eu10.hana.ondemand.com/botHook'.format(APPNAME))
    return str(botWebhookResult)

@route('/botHook', method='POST')
def botHook():
    bot = telegram.Bot(TOKEN)
    update = telegram.update.Update.de_json(request.json, bot)
    bot.sendMessage(chat_id=update.message.chat_id, text=TM_Bot())

def TM_Bot():
    Bot = TaskManagerBot()
    Bot.bot_run()

if __name__ == '__main__':
    run(host='0.0.0.0', port=port)

# run https://taskmanagerbot.cfapps.eu10.hana.ondemand.com/setWebhook to start bot
# run https://facemetricdatasaver.cfapps.eu10.hana.ondemand.com/setWebhook
